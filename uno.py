"""
Welcome to a Pass & Play version of UNO.

The game has no affiliation with Mattel Games, the original creators of UNO.
The game is a project I am devoted to help improve my python skills while
creating a fun game many of my friends know of. I own zero of the rights to
UNO and do not intend to make money off of the project as this is stealing
from Mattel Games. This project aims to be a pass & play version of the
popular online version made by Ubisoft, however once again, I am not
affiliated with Ubisoft and their work.

GitLab: https://gitlab.com/hamish555

Thank you for supporting my project.
"""

# Importing all required libraries, installing those the user does not have
while True:
    try:
        import pip
        import os
        import pathlib
        import random
        import tkinter as tk
        import json
        from PIL import Image, ImageTk
        from console.utils import wait_key
        break
    except ImportError:
        print('Installing Packages Required For UNO')
        pip.main(['install', 'os'])
        pip.main(['install', 'random'])
        pip.main(['install', 'python3-tk'])
        pip.main(['install', 'json'])
        pip.main(['install', 'pillow'])
        pip.main(['install', 'console'])

# Setting working directory path for every OS
os.chdir(pathlib.Path(__file__).parent.resolve())


# Clear function to clear the screen based on the users system
def clear():
    """Do this to clear the screen."""
    if os.name == 'posix':
        os.system('clear')
    else:
        os.system('cls')


# Start function
def start():
    """Do this to start game."""
    # Global variables between TK interfaces
    global root, players, devmode, card, color_check, name_check, \
        stacking_check, continued_drawing_check, error, color
    try:
        # Getting state of each game option
        players = players.get()
        devmode = devmode.get()
        color_check = color_check.get()
        name_check = name_check.get()
        stacking_check = stacking_check.get()
        continued_drawing_check = continued_drawing_check.get()
    except tk.TclError:
        # If no players are entered, program will display this
        error['text'] = 'Please Select Number of Players'
    else:
        # Saving the state of each game option in a JSON file for next round
        file = open('settings.json', 'r')
        data = json.load(file)
        data['devmode'] = devmode
        data['colors'] = color_check
        data['name_check'] = name_check
        data['stacking'] = stacking_check
        data['continued_drawing'] = continued_drawing_check
        json.dump(data, open('settings.json', 'w'))
        root.destroy()
        # Checking if names are enabled so that names can be entered
        if name_check is True:
            # Creates a function which is only needed for names
            def set_names(names):
                names_list = {}
                i = 0
                # Gets name inputs and saves them in a dictionary
                for name in names:
                    names_list[str(i)] = name.get()
                    i += 1
                # Saves / Updates new names to JSON file for next round
                file = open('settings.json', 'r')
                data = json.load(file)
                data['names'].update(names_list)
                json.dump(data, open('settings.json', 'w'))

                root.destroy()

            player_names = []
            file = open('settings.json', 'r')
            data = json.load(file)

            # Creates a TK interface for entering names
            root = tk.Tk()
            root.title('UNO')
            root.geometry('500x500')
            root['bg'] = 'white'

            # Generates a series of Entry boxes based on number of players
            tk.Label(root, text='Add Names:', bg='white').pack(pady=5)
            for i in range(players):
                entryText = tk.StringVar()
                tk.Label(root, text=f'Player {i+1}', bg='white').pack()
                player_name = tk.Entry(root, textvariable=entryText)
                try:
                    entryText.set(data['names'][str(i)])
                except KeyError:
                    pass
                player_name.pack()
                player_names.append(player_name)
            # A button that sends entered names to the name function
            tk.Button(root, text='Start!',
                      command=lambda x=player_names: set_names(x),
                      width=10, bd=0, highlightthickness=0,
                      bg='white').pack(pady=5)

            root.mainloop()

        # Creating card variables
        colors = ['Red', 'Green', 'Blue', 'Yellow']
        cards = []
        playedcards = []

        # Creating all the colored cards 0-9 x2
        for i in range(2):
            num = 0
            for i in range(10):
                num = str(num)
                cards.append(colors[0] + ' ' + num)
                cards.append(colors[1] + ' ' + num)
                cards.append(colors[2] + ' ' + num)
                cards.append(colors[3] + ' ' + num)
                num = int(num)
                num += 1

        # Remove four zeros since each deck only has four zeros
        cards.remove('Red 0')
        cards.remove('Green 0')
        cards.remove('Blue 0')
        cards.remove('Yellow 0')

        # Creating all special colored cards x2
        for i in range(2):
            num = 0
            for i in range(3):
                if num == 0:
                    special = 'skip'
                elif num == 1:
                    special = 'reverse'
                else:
                    special = 'pickup 2'
                cards.append(colors[0] + ' ' + special)
                cards.append(colors[1] + ' ' + special)
                cards.append(colors[2] + ' ' + special)
                cards.append(colors[3] + ' ' + special)
                num += 1

        # Creating other cards
        for i in range(4):
            cards.append('Wild')
            cards.append('Wild pickup 4')

        # Randomly selecting 7 cards and assigning them for each player
        playercards = {}
        player = 0
        for i in range(players):
            player += 1
            cardholder = []
            for i in range(7):
                choice = random.choice(cards)
                cards.remove(choice)
                cardholder.append(choice)
            playercards[player] = cardholder

        # Randomly selecting a starting card
        topcard = random.choice(cards)
        cards.remove(topcard)
        while True:
            if topcard[0] == 'W':
                if 'pickup 4' in topcard:
                    cards.append('Wild pickup 4')
                    topcard = random.choice(cards)
                    cards.remove(topcard)
                else:
                    starting_wild = True
                    topwild = topcard
                    # choice = random.choice(colors)
                    # topcard = choice + ' wild'
                    break
            else:
                starting_wild = False
                break

        # Starting the game
        try:
            # Starting variables / Default variables / Reading JSON file
            player = random.randint(1, players)
            next_player = player
            play = 0
            stack2 = 0
            stack4 = 0
            player_dir = 'Clockwise'
            picking_up = False
            new_cards = {}
            for i in range(players):
                new_cards[i+1] = []
            file = open('settings.json', 'r')
            data = json.load(file)
            # Main play loop
            while True:
                # Checking to make sure the next player is within the limit
                if player == players + 1:
                    player = 1
                elif player == players + 2:
                    player = 2
                elif player == 0:
                    player = players
                elif player < 0:
                    player = players - 1
                # Predicting the next player based on play direction
                next_player = player
                if player_dir == 'Clockwise':
                    next_player += 1
                else:
                    next_player -= 1
                if next_player == players + 1:
                    next_player = 1
                elif next_player == players + 2:
                    next_player = 2
                elif next_player == 0:
                    next_player = players
                elif next_player < 0:
                    next_player = players - 1
                # Sets current player incase player variable is changed later
                currentplayer = player
                clear()
                # Shuffle deck with old cards to continue playing
                if len(cards) <= 0:
                    cards = cards + playedcards
                    playedcards = []
                    if devmode is True:
                        print('Developer Mode Enabled\n')
                    print('Shuffled Cards')
                    print('Press Any Key To Continue')
                    key = wait_key()
                    if key == 'q':
                        clear()
                        quit()
                    clear()
                # Hide cards screen (Will skip if player is picking up)
                if picking_up is False:
                    # Check if devmode is enabled
                    if devmode is True:
                        print('Developer Mode Enabled\n')
                    print('Press \'Q\' To Quit\n')
                    # Uses name if names are enabled or uses player number
                    if name_check is True:
                        print('Current Player:', data['names'][str(player-1)])
                        print('Press Any Key To Show Player',
                              data['names'][str(player-1)] +
                              '\'s Cards')
                    else:
                        print('Current Player:', player)
                        print('Press Any Key To Show Player', str(player) +
                              '\'s Cards')
                    # Wait for any key to be pressed
                    key = wait_key()
                    # If key press is 'q' quit game
                    if key == 'q':
                        clear()
                        quit()
                clear()
                # Prints if devmode is enabled
                if devmode is True:
                    print('Developer Mode Enabled\n')
                # Tells the user to quit button is 'q'
                print('Press \'Q\' To Quit\n')
                # Prints current players name or number depending whats enabled
                if name_check is True:
                    print('Current Player:', data['names'][str(player-1)])
                else:
                    print('Current Player:', player)
                # Prints the direction of the game
                print('Direction:', player_dir)
                # Prints next players name or number depending whats enabled
                if name_check is True:
                    print('Predicted Next Player:',
                          data['names'][str(next_player-1)])
                else:
                    print('Predicted Next Player:', next_player)
                # Prints if the current player has nenw cards in their hand
                if len(new_cards[player]) == 1:
                    print('New Card:', ', '.join(new_cards[player]))
                elif len(new_cards[player]) > 1:
                    print('New Cards:', ', '.join(new_cards[player]))
                # Prints if there is a current stack of cards
                if stack2 > 0 or stack4 > 0:
                    print('Stacking:', stack2 + stack4)
                # Reseting the new cards for the current player
                new_cards[player] = []
                # Sorting cards into RGBYW formation for the current player
                red_cards = []
                green_cards = []
                blue_cards = []
                yellow_cards = []
                wild_cards = []
                for i in range(len(playercards[player])):
                    if playercards[player][i][0] == 'R':
                        red_cards.append(playercards[player][i])
                    elif playercards[player][i][0] == 'G':
                        green_cards.append(playercards[player][i])
                    elif playercards[player][i][0] == 'B':
                        blue_cards.append(playercards[player][i])
                    elif playercards[player][i][0] == 'Y':
                        yellow_cards.append(playercards[player][i])
                    elif playercards[player][i][0] == 'W':
                        wild_cards.append(playercards[player][i])
                red_cards.sort()
                green_cards.sort()
                blue_cards.sort()
                yellow_cards.sort()
                wild_cards.sort()
                playercards[player] = red_cards + green_cards + blue_cards \
                    + yellow_cards + wild_cards
                # Remaking the 'Pickup card' in the players hand
                if len(cards) > 0:
                    if playercards[player].__contains__('Pickup card'):
                        playercards[player].remove('Pickup card')
                        playercards[player].append('Pickup card')
                    else:
                        playercards[player].append('Pickup card')
                if starting_wild is True:
                    starting_wild = False

                    def select_color(sel_color):
                        global color
                        color = sel_color
                        root.destroy()
                    while True:
                        clear()
                        if devmode is True:
                            print('Developer Mode Enabled\n')
                        print('Press \'Q\' To Quit\n')
                        if name_check is True:
                            print('Current Player:',
                                  data['names'][str(player-1)])
                        else:
                            print('Current Player:', player)
                        print('Direction:', player_dir)
                        if name_check is True:
                            print('Predicted Next Player:',
                                  data['names'][str(next_player-1)])
                        else:
                            print('Predicted Next Player:',
                                  next_player)
                        if len(new_cards[player]) == 1:
                            print('New Card:',
                                  ', '.join(new_cards[player]))
                        elif len(new_cards[player]) > 1:
                            print('New Cards:',
                                  ', '.join(new_cards[player]))
                        if stack2 > 0 or stack4 > 0:
                            print('Stacking:', stack2 + stack4)

                        root = tk.Tk()
                        root.geometry('500x500')
                        root.title('UNO')

                        tk.Label(root, text=('What Color Do You '
                                             'Want to start?')).pack()
                        red_button = tk.Button(root, text='Red',
                                               command=lambda x=0:
                                               select_color('red'),
                                               width=5)
                        red_button.pack()
                        blue_button = tk.Button(root, text='Blue',
                                                command=lambda x=0:
                                                select_color('blue'),
                                                width=5)
                        blue_button.pack()
                        green_button = tk.Button(root, text='Green',
                                                 command=lambda x=0:
                                                 select_color('green'),
                                                 width=5)
                        green_button.pack()
                        yellow_button = tk.Button(root, text='Yellow',
                                                  command=lambda x=0:
                                                  select_color
                                                  ('yellow'), width=5)
                        yellow_button.pack()
                        tk.Label(root, text='Your Cards:').pack()
                        graph_cards = playercards[player]
                        scroll_bar = tk.Scrollbar(root, width=15)
                        card_list = tk.Listbox(root,
                                               yscrollcommand=scroll_bar.set,
                                               width=20)
                        red_cards = []
                        blue_cards = []
                        green_cards = []
                        yellow_cards = []
                        for i in range(len(graph_cards)):
                            i -= 1
                            if color_check is True:
                                if graph_cards[i][0] == 'R':
                                    red_cards.append(i)
                                elif graph_cards[i][0] == 'B':
                                    blue_cards.append(i)
                                elif graph_cards[i][0] == 'G':
                                    green_cards.append(i)
                                elif graph_cards[i][0] == 'Y':
                                    yellow_cards.append(i)
                            if graph_cards[i] != 'Pickup card':
                                card_list.insert(tk.END,
                                                 graph_cards[i])
                        if color_check is True:
                            for i in red_cards:
                                card_list.itemconfig(i, {'fg': 'red'})
                            for i in blue_cards:
                                card_list.itemconfig(i, {'fg': 'blue'})
                            for i in green_cards:
                                card_list.itemconfig(i,
                                                     {'fg': 'green'})
                            for i in yellow_cards:
                                card_list.itemconfig(i, {'fg': 'gold'})
                        scroll_bar.pack(side=tk.RIGHT, fill=tk.Y)
                        card_list.pack(side=tk.RIGHT, fill=tk.BOTH,
                                       ipadx=999)
                        scroll_bar.config(command=card_list.yview)
                        if color_check is True:
                            red_button['fg'] = 'red'
                            green_button['fg'] = 'green'
                            blue_button['fg'] = 'blue'
                            yellow_button['fg'] = 'gold'
                        root.mainloop()
                        try:
                            if color:
                                if color[0] == 'r':
                                    topcard = 'Red wild'
                                elif color[0] == 'y':
                                    topcard = 'Yellow wild'
                                elif color[0] == 'g':
                                    topcard = 'Green wild'
                                elif color[0] == 'b':
                                    topcard = 'Blue wild'
                                else:
                                    print('\nUnkown Color')
                                    print('Press Any Key To Continue')
                                    key = wait_key()
                                    if key == 'q':
                                        clear()
                                        quit()
                                    clear()
                            break
                        except NameError:
                            print('\nEnter Color')
                            print('Press Any Key To Continue')
                            key = wait_key()
                            if key == 'q':
                                clear()
                                quit()
                            clear()
                # Sets the current card to be blank
                card = ''

                # Creates a function to get the played card
                def card_sel(x):
                    """Do this to select the card in play."""
                    global card
                    try:
                        # Get the card and close the GUI
                        card = card_list.get(card_list.curselection())
                        root.destroy()
                    except tk.TclError:
                        pass

                # Creates the GUI for the current player to select a card
                root = tk.Tk()
                root.geometry('500x500')
                root.title('UNO')

                # Displaying the top card as its color if colors are enabled
                if color_check is True:
                    if topcard[0] == 'R':
                        tk.Label(root, text='Top card: ' + topcard, fg='red')\
                            .pack()
                    elif topcard[0] == 'G':
                        tk.Label(root, text='Top card: ' + topcard,
                                 fg='green').pack()
                    elif topcard[0] == 'B':
                        tk.Label(root, text='Top card: ' + topcard,
                                 fg='blue').pack()
                    elif topcard[0] == 'Y':
                        tk.Label(root, text='Top card: ' + topcard, fg='gold')\
                            .pack()
                    else:
                        tk.Label(root, text='Top card: ' + topcard).pack()
                # Displays card normally if colors are disabled
                else:
                    tk.Label(root, text='Top card: ' + topcard).pack()
                # Displaying the current number of cards each player has
                for i in range(players - 1):
                    i += 1
                    if name_check is True:
                        player_name = data['names'][str(i-1)]
                    else:
                        player_name = i
                    if playercards[i].__contains__('Pickup card'):
                        text = 'Player ' + str(player_name) + ': ' + \
                               str(len(playercards[i]) - 1)
                        tk.Label(root, text=text).pack()
                    else:
                        text = 'Player ' + str(player_name) + ': ' +\
                            str(len(playercards[i]))
                        tk.Label(root, text=text).pack()
                # Fixing sync issue with last player in the list
                i += 1
                if name_check is True:
                    player_name = data['names'][str(i-1)]
                else:
                    player_name = i
                if playercards[i].__contains__('Pickup card'):
                    text = 'Player ' + str(player_name) + ': ' +\
                        str(len(playercards[i]) - 1)
                    tk.Label(root, text=text).pack()
                else:
                    text = 'Player ' + str(player_name) + ': ' + \
                        str(len(playercards[i]))
                    tk.Label(root, text=text).pack()
                # Setting up the Listbox to display the current players cards
                graph_cards = playercards[player]
                scroll_bar = tk.Scrollbar(root, width=15)
                card_list = tk.Listbox(root, yscrollcommand=scroll_bar.set,
                                       width=20)
                # Setting the option to double click with Left or Right click
                card_list.bind('<Double-1>', card_sel)
                card_list.bind('<Double-3>', card_sel)
                # Setting the option to press Enter/Return
                card_list.bind('<Return>', card_sel)
                # Setting up lists to store cards to display as color
                red_cards = []
                blue_cards = []
                green_cards = []
                yellow_cards = []
                for i in range(len(graph_cards)):
                    i -= 1
                    if color_check is True:
                        if graph_cards[i][0] == 'R':
                            red_cards.append(i)
                        elif graph_cards[i][0] == 'B':
                            blue_cards.append(i)
                        elif graph_cards[i][0] == 'G':
                            green_cards.append(i)
                        elif graph_cards[i][0] == 'Y':
                            yellow_cards.append(i)
                    card_list.insert(tk.END, graph_cards[i])
                if color_check is True:
                    for i in red_cards:
                        if graph_cards.__contains__('Pickup card'):
                            card_list.itemconfig(i + 1, {'fg': 'red'})
                        else:
                            card_list.itemconfig(i + 2, {'fg': 'red'})
                    for i in blue_cards:
                        if graph_cards.__contains__('Pickup card'):
                            card_list.itemconfig(i + 1, {'fg': 'blue'})
                        else:
                            card_list.itemconfig(i + 2, {'fg': 'blue'})
                    for i in green_cards:
                        if graph_cards.__contains__('Pickup card'):
                            card_list.itemconfig(i + 1, {'fg': 'green'})
                        else:
                            card_list.itemconfig(i + 2, {'fg': 'green'})
                    for i in yellow_cards:
                        if graph_cards.__contains__('Pickup card'):
                            card_list.itemconfig(i + 1, {'fg': 'gold'})
                        else:
                            card_list.itemconfig(i + 2, {'fg': 'gold'})
                scroll_bar.pack(side=tk.RIGHT, fill=tk.Y)
                card_list.pack(side=tk.RIGHT, fill=tk.BOTH, ipadx=999)
                scroll_bar.config(command=card_list.yview)

                root.mainloop()
                cardholder = ''
                num = 1
                for i in range(len(card) - 1):
                    cardholder = cardholder + card[num].lower()
                    num += 1
                if card:
                    card = card[0].upper()
                    card = card + cardholder
                if playercards[player].__contains__(card):
                    play = 1
                    if card[0] == topcard[0] or card[-1] == topcard[-1]:
                        if 'pickup 2' in topcard and card[-1] == '2':
                            if card[0] == topcard[0]:
                                pass
                            elif card[-3] == 'p':
                                pass
                            else:
                                print('\nCant Play Card')
                                print('Press Any Key To Continue')
                                key = wait_key()
                                if key == 'q':
                                    clear()
                                    quit()
                                play = 0
                                clear()
                        elif 'pickup 2' in card:
                            if card[0] == topcard[0]:
                                pass
                            else:
                                print('\nCant Play Card')
                                print('Press Any Key To Continue')
                                key = wait_key()
                                if key == 'q':
                                    clear()
                                    quit()
                                play = 0
                                clear()
                    elif card == 'Pickup card':
                        pass
                    elif card[0] == 'W':
                        pass
                    else:
                        print('\nCant Play Card')
                        print('Press Any Key To Continue')
                        key = wait_key()
                        if key == 'q':
                            clear()
                            quit()
                        play = 0
                        clear()
                    if stacking_check is True and play == 1:
                        if card == 'Pickup card':
                            play = 1
                        elif stack2 > 0 and 'pickup 2' in card:
                            play = 1
                        elif stack2 > 0 and 'pickup 2' not in card:
                            print('\nCant Play Card')
                            print('Press Any Key To Continue')
                            key = wait_key()
                            if key == 'q':
                                clear()
                                quit()
                            play = 0
                            clear()
                        elif stack4 > 0 and 'pickup 4' in card:
                            play = 1
                        elif stack4 > 0 and 'pickup 4' not in card:
                            print('\nCant Play Card')
                            print('Press Any Key To Continue')
                            key = wait_key()
                            if key == 'q':
                                clear()
                                quit()
                            play = 0
                            clear()
                    if play == 1:
                        playercards[player].remove(card)
                        if 'reverse' in card:
                            picking_up = False
                            if player_dir == 'Clockwise':
                                player_dir = 'Anticlcokwise'
                            else:
                                player_dir = 'Clockwise'
                            next_player = player
                            if player_dir == 'Clockwise':
                                next_player += 1
                            else:
                                next_player -= 1
                            if next_player == players + 1:
                                next_player = 1
                            elif next_player == players + 2:
                                next_player = 2
                            elif next_player == 0:
                                next_player = players
                            elif next_player < 0:
                                next_player = players - 1
                            if players == 2:
                                if player == 1:
                                    player = 2
                                else:
                                    player = 1
                        if 'skip' in card:
                            picking_up = False
                            next_player = player
                            if player_dir == 'Clockwise':
                                next_player += 1
                            else:
                                next_player -= 1
                            if next_player == players + 1:
                                next_player = 1
                            elif next_player == players + 2:
                                next_player = 2
                            elif next_player == 0:
                                next_player = players
                            elif next_player < 0:
                                next_player = players - 1
                            if player_dir == 'Clockwise':
                                player += 1
                            else:
                                player -= 1
                        if 'pickup 2' in card:
                            if stacking_check is True:
                                stack2 += 2
                            else:
                                picking_up = False
                                next_player = player
                                if player_dir == 'Clockwise':
                                    next_player += 1
                                else:
                                    next_player -= 1
                                if next_player == players + 1:
                                    next_player = 1
                                elif next_player == players + 2:
                                    next_player = 2
                                elif next_player == 0:
                                    next_player = players
                                elif next_player < 0:
                                    next_player = players - 1
                                if player_dir == 'Clockwise':
                                    player += 1
                                else:
                                    player -= 1
                                if player == players + 1:
                                    player = 1
                                elif player == 0:
                                    player = players
                                if len(cards) > 1:
                                    for i in range(2):
                                        choice = random.choice(cards)
                                        cards.remove(choice)
                                        playercards[player].append(choice)
                                        new_cards[player].append(choice)
                                else:
                                    pass
                        if 'Wild' in card:
                            def select_color(sel_color):
                                global color
                                color = sel_color
                                root.destroy()
                            topwild = card
                            if 'pickup 4' in card:
                                if stacking_check is True:
                                    stack4 += 4
                                else:
                                    next_player = player
                                    if player_dir == 'Clockwise':
                                        next_player += 1
                                    else:
                                        next_player -= 1
                                    if next_player == players + 1:
                                        next_player = 1
                                    elif next_player == players + 2:
                                        next_player = 2
                                    elif next_player == 0:
                                        next_player = players
                                    elif next_player < 0:
                                        next_player = players - 1
                            while True:
                                clear()
                                if devmode is True:
                                    print('Developer Mode Enabled\n')
                                print('Press \'Q\' To Quit\n')
                                if name_check is True:
                                    print('Current Player:',
                                          data['names'][str(player-1)])
                                else:
                                    print('Current Player:', player)
                                print('Direction:', player_dir)
                                if name_check is True:
                                    print('Predicted Next Player:',
                                          data['names'][str(next_player-1)])
                                else:
                                    print('Predicted Next Player:',
                                          next_player)
                                if len(new_cards[player]) == 1:
                                    print('New Card:',
                                          ', '.join(new_cards[player]))
                                elif len(new_cards[player]) > 1:
                                    print('New Cards:',
                                          ', '.join(new_cards[player]))
                                if stack2 > 0 or stack4 > 0:
                                    print('Stacking:', stack2 + stack4)
                                precard = card

                                root = tk.Tk()
                                root.geometry('500x500')
                                root.title('UNO')

                                tk.Label(root, text='What Color Do You Want?')\
                                    .pack()
                                red_button = tk.Button(root, text='Red',
                                                       command=lambda x=0:
                                                       select_color('red'),
                                                       width=5)
                                red_button.pack()
                                blue_button = tk.Button(root, text='Blue',
                                                        command=lambda x=0:
                                                        select_color('blue'),
                                                        width=5)
                                blue_button.pack()
                                green_button = tk.Button(root, text='Green',
                                                         command=lambda x=0:
                                                         select_color('green'),
                                                         width=5)
                                green_button.pack()
                                yellow_button = tk.Button(root, text='Yellow',
                                                          command=lambda x=0:
                                                          select_color
                                                          ('yellow'), width=5)
                                yellow_button.pack()
                                tk.Label(root, text='Your Cards:').pack()
                                graph_cards = playercards[player]
                                scroll_bar = tk.Scrollbar(root, width=15)
                                card_list = \
                                    tk.Listbox(root,
                                               yscrollcommand=scroll_bar.set,
                                               width=20)
                                red_cards = []
                                blue_cards = []
                                green_cards = []
                                yellow_cards = []
                                for i in range(len(graph_cards)):
                                    i -= 1
                                    if color_check is True:
                                        if graph_cards[i][0] == 'R':
                                            red_cards.append(i)
                                        elif graph_cards[i][0] == 'B':
                                            blue_cards.append(i)
                                        elif graph_cards[i][0] == 'G':
                                            green_cards.append(i)
                                        elif graph_cards[i][0] == 'Y':
                                            yellow_cards.append(i)
                                    if graph_cards[i] != 'Pickup card':
                                        card_list.insert(tk.END,
                                                         graph_cards[i])
                                if color_check is True:
                                    for i in red_cards:
                                        card_list.itemconfig(i, {'fg': 'red'})
                                    for i in blue_cards:
                                        card_list.itemconfig(i, {'fg': 'blue'})
                                    for i in green_cards:
                                        card_list.itemconfig(i,
                                                             {'fg': 'green'})
                                    for i in yellow_cards:
                                        card_list.itemconfig(i, {'fg': 'gold'})
                                scroll_bar.pack(side=tk.RIGHT, fill=tk.Y)
                                card_list.pack(side=tk.RIGHT, fill=tk.BOTH,
                                               ipadx=999)
                                scroll_bar.config(command=card_list.yview)
                                if color_check is True:
                                    red_button['fg'] = 'red'
                                    green_button['fg'] = 'green'
                                    blue_button['fg'] = 'blue'
                                    yellow_button['fg'] = 'gold'
                                root.mainloop()
                                try:
                                    if color:
                                        if color[0] == 'r':
                                            card = 'Red wild'
                                        elif color[0] == 'y':
                                            card = 'Yellow wild'
                                        elif color[0] == 'g':
                                            card = 'Green wild'
                                        elif color[0] == 'b':
                                            card = 'Blue wild'
                                        else:
                                            print('\nUnkown Color')
                                            print('Press Any Key To Continue')
                                            key = wait_key()
                                            if key == 'q':
                                                clear()
                                                quit()
                                            clear()
                                    if 'pickup 4' in precard:
                                        if stacking_check is True:
                                            pass
                                        else:
                                            picking_up = False
                                            if player_dir == 'Clockwise':
                                                player += 1
                                            else:
                                                player -= 1
                                            if player == players + 1:
                                                player = 1
                                            elif player == 0:
                                                player = players
                                            if len(cards) > 3:
                                                for i in range(4):
                                                    choice = random\
                                                        .choice(cards)
                                                    cards.remove(choice)
                                                    playercards[player]\
                                                        .append(choice)
                                                    new_cards[player]\
                                                        .append(choice)
                                    break
                                except NameError:
                                    print('\nEnter Color')
                                    print('Press Any Key To Continue')
                                    key = wait_key()
                                    if key == 'q':
                                        clear()
                                        quit()
                                    clear()
                        if card == 'Pickup card':
                            if stack2 > 0 or stack4 > 0:
                                for _ in range(stack2 + stack4):
                                    choice = random.choice(cards)
                                    cards.remove(choice)
                                    playercards[player].append(choice)
                                    new_cards[player].append(choice)
                                stack2, stack4 = 0, 0
                            else:
                                choice = random.choice(cards)
                                cards.remove(choice)
                                playercards[player].append(choice)
                                new_cards[player].append(choice)
                                if continued_drawing_check is True:
                                    picking_up = True
                                    if player_dir == 'Clockwise':
                                        player -= 1
                                    else:
                                        player += 1
                                    next_player = player
                                    if player_dir == 'Clockwise':
                                        next_player += 1
                                    else:
                                        next_player -= 1
                                    if next_player == players + 1:
                                        next_player = 1
                                    elif next_player == players + 2:
                                        next_player = 2
                                    elif next_player == 0:
                                        next_player = players
                                    elif next_player < 0:
                                        next_player = players - 1
                        else:
                            if 'wild' in topcard:
                                playedcards.append(topwild)
                            else:
                                playedcards.append(topcard)

                            if picking_up is True:
                                if player_dir == 'Clockwise':
                                    player = next_player - 1
                                else:
                                    player = next_player + 1
                            picking_up = False
                            if picking_up is False:
                                topcard = card
                            try:
                                if player > players:
                                    playercards[player - 1]\
                                        .remove('Pickup card')
                                elif player < players:
                                    playercards[player + 1]\
                                        .remove('Pickup card')
                                else:
                                    playercards[player].remove('Pickup card')
                            except ValueError:
                                pass
                        if player_dir == 'Clockwise':
                            player += 1
                        else:
                            player -= 1
                        if len(playercards[currentplayer]) == 1:
                            if 'Pickup card' in playercards[currentplayer]:
                                clear()
                                if name_check is True:
                                    print(data['names'][str(currentplayer -
                                                            1)], 'Wins')
                                else:
                                    print('Player', str(currentplayer), 'Wins')
                                print('Press Any Key To End')
                                key = wait_key()
                                if key == 'q':
                                    clear()
                                    quit()
                                clear()
                                quit()
                        else:
                            if len(playercards[currentplayer]) == 0:
                                clear()
                                if name_check is True:
                                    print(data['names'][str(currentplayer -
                                                            1)], 'Wins')
                                else:
                                    print('Player', str(currentplayer), 'Wins')
                                print('Press Any Key To End')
                                key = wait_key()
                                if key == 'q':
                                    clear()
                                    quit()
                                clear()
                                quit()
                else:
                    next_player = player
                    print('\nDont Close The Window')
                    print('Press Any Key To Continue')
                    key = wait_key()
                    if key == 'q':
                        clear()
                        quit()
                    clear()
        except (EOFError, KeyboardInterrupt):
            clear()


def menu():
    """Do this to show starting menu."""
    global root, players, devmode, color_check, name_check, stacking_check, \
        continued_drawing_check, play_draw_check, error
    file = open('settings.json', 'r')
    data = json.load(file)
    options = [i for i in range(2, 11)]

    root = tk.Tk()
    root['bg'] = 'white'
    root.title('UNO')

    players = tk.IntVar()
    players.set('Select Number of Players')

    devmode = tk.BooleanVar(root)
    devmode.set(data['devmode'])

    color_check = tk.BooleanVar(root)
    color_check.set(data['colors'])

    name_check = tk.BooleanVar(root)
    name_check.set(data['name_check'])

    stacking_check = tk.BooleanVar(root)
    stacking_check.set(data['stacking'])

    continued_drawing_check = tk.BooleanVar(root)
    continued_drawing_check.set(data['continued_drawing'])

    uno_photo = Image.open('./media/uno.png')
    uno_photo = uno_photo.resize((300, 200))
    uno_photo = ImageTk.PhotoImage(uno_photo)
    uno_photo_label = tk.Label(image=uno_photo, bd=0, highlightthickness=0)
    uno_photo_label.pack(pady=25, padx=100)

    player_drop_down = tk.OptionMenu(root, players, *options)
    player_drop_down.config(bg='white', bd=0, highlightthickness=0)
    player_drop_down['menu'].config(bg='white', bd=0,)
    player_drop_down.pack()

    dev_check = tk.Checkbutton(root, variable=devmode, text='Developer Mode',
                               bd=0, highlightthickness=0, bg='white')
    dev_check.pack()

    color_check_box = tk.Checkbutton(root, variable=color_check,
                                     text='Enable Colors', bd=0,
                                     highlightthickness=0, bg='white')
    color_check_box.pack()

    name_check_box = tk.Checkbutton(root, variable=name_check,
                                    text='Enable Names', bd=0,
                                    highlightthickness=0, bg='white')
    name_check_box.pack()

    stacking_check_box = tk.Checkbutton(root, variable=stacking_check,
                                        text='Enable Stacking +2 or +4', bd=0,
                                        highlightthickness=0, bg='white')
    stacking_check_box.pack()

    continued_drawing_check_box = \
        tk.Checkbutton(root, variable=continued_drawing_check,
                       text=('Enable Drawing Until You Can Play'), bd=0,
                       highlightthickness=0,
                       bg='white')
    continued_drawing_check_box.pack()

    start_btn = tk.Button(root, text='Start!', width=10, command=start, bd=0,
                          highlightthickness=0, bg='white')
    start_btn.pack(ipady=5, pady=5)

    error = tk.Label(root, fg='red', text='', bg='white')
    error.pack()

    root.mainloop()

if __name__ == '__main__':
    clear()
    menu()
